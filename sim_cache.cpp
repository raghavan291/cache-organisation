#include<iostream>
#include<cstdlib>
#include<fstream>
#include<string>
#include<string.h>
#include<sstream>
#include"cache.h"

using namespace std;

#define DEBUG 1
		
void usage()
{
	cout<<"Incorrect command line arguments, enter as follows"<<endl;
	cout<<"sim_cache <BLOCKSIZE> <L1_SIZE> <L1_ASSOC> <PREF_N> <PREF_M> <L2_SIZE> <L2_ASSOC> <trace_file>"<<endl;
	cout<<"where"<<endl;
	cout<<"\t<BLOCKSIZE> - Block size in bytes"<<endl;
	cout<<"\t<L1_SIZE> - Total L1 CACHE size in bytes"<<endl;
	cout<<"\t<L2_SIZE> - Total L2 CACHE size in bytes. 0 signifies L2 is disabled"<<endl;
	cout<<"\t<L1_ASSOC> - Associativity of L1 CACHE"<<endl;
	cout<<"\t<L2_ASSOC> - Associativity of the L2 CACHE. 0 represent L2 disabled"<<endl;
	cout<<"\t<PREF_N> - Number of stream buffers. 0 signifies prefetch unit is disabled"<<endl;
	cout<<"\t<PREF_M> - Number memory blocks in each stream buffer"<<endl;
	cout<<"\t<Trace_file> - Full name of the trace file including any extensions"<<endl;

}


int main(int argc, char *argv[])
{
	int i;
	bool miss_status;
	int L1_tag_value, L2_tag_value, L1_index_l, L2_index_l;
	string address1, address2, file_line;
	char op;
	if (argc == 9)
	{
		CACHE L1(atoi(argv[1]), atoi(argv[2]), atoi(argv[3]), atoi(argv[4]), atoi(argv[5]), argv[8], 1);	
		CACHE L2(atoi(argv[1]), atoi(argv[6]), atoi(argv[7]), atoi(argv[4]), atoi(argv[5]), argv[8], 2);	
	
		cout<<"===== Simulator configuration ====="<<endl;
	        cout<<"BLOCKSIZE:                    "<<argv[1]<<endl;
        	cout<<"L1_SIZE:                       "<<argv[2]<<endl;
        	cout<<"L1_ASSOC:                         "<<argv[3]<<endl;
        	cout<<"PREF_N:                           "<<argv[4]<<endl;
        	cout<<"PREF_M:                           "<<argv[5]<<endl;
        	cout<<"L2_SIZE:                          "<<argv[6]<<endl;
        	cout<<"L2_ASSOC:                         "<<argv[7]<<endl;
        	cout<<"trace_file:           "<<argv[8]<<endl;

		L1.compute_blockoffset();
		L1.compute_sets();
		L1.compute_index();
		L1.compute_tagsize();
		L1.allocate_tag_set();
		L1.allocate_streambuffers();
		for(i = 0; i < L1.SETS; i++)
		{
			L1.t_set[i].create_tagset(L1.ASSOC);
		}
		L1.initialise_tagstore();		
		for(i = 0; i < L1.PREF_N; i++)
		{
			L1.stbuf[i].create_streambuffer(L1.PREF_M);
		}
		L1.initialise_streambuffers();	
		L1.initialise_stats();

		if (atoi(argv[6]) != 0 && atoi(argv[7]) != 0)
		{
			L2.compute_blockoffset();
                	L2.compute_sets();
                	L2.compute_index();
                	L2.compute_tagsize();
                	L2.allocate_tag_set();
                	for(i = 0; i < L2.SETS; i++)
                	{
                        	L2.t_set[i].create_tagset(L2.ASSOC);
                	}
                	L2.initialise_tagstore();
			L2.initialise_stats();
		}
		L1.nextLevel = &L2;
		L2.nextLevel = NULL;	

		ifstream tracefile(argv[8]);
        	if (tracefile.is_open()) 
        	{
			while (getline(tracefile, file_line))
			{	 
	        	#ifdef DEBUG
				cout<<file_line<<endl; 
			#endif
				op = file_line.at(0);	
				address1 = file_line.substr(2);
				address2 = file_line.substr(2);
			#ifdef DEBUG
                                cout<<address1<<endl; 
				cout<<address2<<endl;
                        #endif

				L1_tag_value = L1.manipulate_address(address1, L1_index_l);
				if (atoi(argv[6]) != 0 && atoi(argv[7]) != 0)
					L2_tag_value = L2.manipulate_address(address2, L2_index_l);
        		#ifdef DEBUG
				cout<<"The line of file is "<<file_line<<endl;
				cout<<"The operation is "<<op<<endl;
				ss << hex << tag_value;
				result = ss.str();
				cout<<"The tag address for L1 "<<L1_tag_value<<endl;
				cout<<"The tag address is L2 "<<L2_tag_value<<endl;
				cout<<"The index of for L1 is "<<L1_index_l<<endl;
				cout<<"The index of for L2 is "<<L2_index_l<<endl;
			#endif
				if(op == 'r')
				{
					miss_status = L1.read(L1_tag_value, L1_index_l);
					if (miss_status == false)
						continue;
					else
					{	
						if (atoi(argv[6]) != 0 && atoi(argv[7]) != 0)
						{
							miss_status = L2.read(L2_tag_value, L2_index_l);
							if (miss_status == false)
								continue;
							else
							{
								cout<<"In main memory"<<endl;
							}
						}
					}
					
				}
				else if(op == 'w')
				{
					miss_status = L1.write(L1_tag_value, L1_index_l);
                                        if (miss_status == false)
                                                continue;
                                        else
                                        {
                                                if (atoi(argv[6]) != 0 && atoi(argv[7]) != 0)
						{
                                                	miss_status = L2.read(L2_tag_value, L2_index_l);
							if (miss_status == false)
                                                        	continue;
                                                	else
                                                	{
								cout<<"In main memory"<<end;
                                                	}
						}
                                        }
				}
				else 
				{
					cout<<"Others options"<<endl;
				}
			
			}
			tracefile.close();
		}
		else
		{
			#ifdef DEBUG
				cout<<"Error - Unable to open file"<<endl;
			#endif
		}
		cout<<"===== L1 contents ====="<<endl;
		L1.print_cache_order();
		cout<<"===== L2 contents ====="<<endl;
		L2.print_cache_order();
		L1.print_stats();
	}
	else
	{
		usage();
	}
	return 0;
}

