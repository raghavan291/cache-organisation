CC = g++
#OPT = -O3
OPT = -g
WARN = -Wall -w -W
CFLAGS = $(OPT) $(WARN)

# Source files
SIM_SRC = cache.cpp sim_cache.cpp

# Object files 
SIM_OBJ = cache.o sim_cache.o
 
#################################

# Default target

all: sim_cache

# Creating sim_cache binary

sim_cache: $(SIM_OBJ)
	$(CC) -o sim_cache $(CFLAGS) $(SIM_OBJ) -lm

# Converting any .cpp file to any .o file
 
.cpp.o:
	$(CC) $(CFLAGS) -c $*.cpp


# Removing all .o files plus the sim_cache binary

clean:
	rm -f *.o sim_cache


# Removing all .o files (leaves sim_cache binary)

clobber:
	rm -f *.o
