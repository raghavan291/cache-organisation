#include<iostream>
#include<string>
#include<cmath>
#include<cstdlib>
#include<stdio.h>
#include"cache.h"

using namespace std;

#define DEBUG 1

void CACHE::initialise_stats()
{
	read_miss = 0;
	write_miss = 0;
	num_wb = 0;
	num_reads = 0;
	num_writes = 0;
	wt_memt = 0;
}


bool CACHE::compute_streambuffer(int tag_value, int index)
{
	int i, j, pos;
	bool status = true;
	int stbuf_pos, counter;
	int stbuf_tag_value, stbuf_index;
	int flag = 0, check = 0;
	for(i = 0; i < PREF_N; i++)
	{
		if(stbuf[i].valid_bit_stbuf == 0)
		{
			cout<<"Stream buffer "<<i<<" miss "<<endl;
		}
		else
		{
			flag = 1;
			break;		
		}
	}
	if (flag == 0)
	{
		counter = stbuf[0].LRU_counter;
		stbuf_pos = 0;
		for(i = 0; i < PREF_N; i++)
		{
			if(stbuf[i].LRU_counter > counter)
			{
				stbuf_pos = i;
				counter = stbuf[i].LRU_counter;
			}
		}

		stbuf_tag_value = tag_value<<this->INDEX;
                stbuf_tag_value |= index;
		
		for(i =0; i < PREF_M; i++)
		{
			stbuf[stbuf_pos].stream_tag[i] = stbuf_tag_value+i+1;
			stbuf[stbuf_pos].invalid_bit[i] = 1;
		}
		stbuf[stbuf_pos].valid_bit_stbuf = 1;
		cout<<"Stream buffer miss"<<endl;
		status = true;
		return status;
		
	}
	if (flag == 1)
	{
		for(i=0; i < PREF_N; i++)
		{
			if (stbuf[i].valid_bit_stbuf == 1)
			{
				cout<<"Stream buffer is full";
			}
			else
			{
				break;
				check = 1;
			}
		}
		if (check == 0)
		{
			stbuf_tag_value = tag_value<<this->INDEX;
                        stbuf_tag_value |= index;                
			for (i = 0; i < PREF_N; i++)
			{
				for (j = 0; j <PREF_M; j++)
				{
					if(stbuf[i].stream_tag[j] == stbuf_tag_value)
					{
						cout<<"Stream buffer hit"<<endl;
						pos = j;
						break;
					}
				}
				
				return status;
			}
				

	counter = stbuf[0].LRU_counter;
			                stbuf_pos = 0;
                			for(i = 0; i < PREF_N; i++)
                			{
                        			if(stbuf[i].LRU_counter > counter)
                        			{
                                			stbuf_pos = i;
                                			counter = stbuf[i].LRU_counter;
                        			}
                			}

                			stbuf_tag_value = tag_value<<this->INDEX;
                			stbuf_tag_value += index;

                for(i =0; i < PREF_M; i++)
                {
                        stbuf[stbuf_pos].stream_tag[i] = stbuf_tag_value+i+1;
                        stbuf[stbuf_pos].invalid_bit[i] = 1;
                }
                stbuf[stbuf_pos].valid_bit_stbuf = 1;
                cout<<"Stream buffer miss"<<endl;
                status = true;
                return status;


		}
		
	}
	return status;
}

void TAG_SET::create_tagset(int ASSOC_t)
{
	tag = new int[ASSOC_t];
	counter = new int[ASSOC_t];
	dirty_bit = new bool[ASSOC_t];
	valid_bit = 0; 
	ASSOC = ASSOC_t;
}


void STREAMBUFFER::create_streambuffer(int PREF_M_t)
{
	stream_tag = new int[PREF_M_t];
	invalid_bit = new int[PREF_M_t];
	PREF_M = PREF_M_t;
}


void CACHE::allocate_tag_set()
{
	t_set = new TAG_SET[SETS];
}


void CACHE::allocate_streambuffers()
{
	stbuf = new STREAMBUFFER[PREF_N];
}

bool CACHE::read(int tag_value, int index_l)
{
	bool miss_status;
	miss_status = this->compute_LRU(tag_value, index_l);
	return miss_status;
}


bool CACHE::write(int tag_value, int index_l)
{
        bool miss_status;
        miss_status = this->compute_writeback_allocate(tag_value, index_l);
	return miss_status;
}


bool CACHE::compute_LRU(int tag_value, int index)
{
	int i,j;
	bool status = true;
	int init_counter = t_set[index].counter[0];
	int flag = 0, pos=0;
	int nlevel_tag_value, nlevel_index;
	this->num_reads++;
	
	if(t_set[index].valid_bit == 0)
	{
		for(i = 0; i < ASSOC; i++)
		{
			if(t_set[index].counter[i] > init_counter)
			{
				pos = i;
				init_counter = t_set[index].counter[i];
			}
		}
		t_set[index].tag[pos] = tag_value;
		for (i = 0; i < ASSOC; i++)
		{
			if(i != pos)
			{
				t_set[index].counter[i]++;
			}	
		}
		t_set[index].counter[pos] = 0;
		(t_set[index].valid_bit)++;
		#ifdef DEBUG
			cout<<"Read (LRU) - Cache Miss"<<endl;
		#endif 
			status = true;
			this->read_miss++;
		
	}
	else if(t_set[index].valid_bit == ASSOC)
	{
		for(i = 0; i < ASSOC; i++)
		{
                	if(t_set[index].tag[i] ==  tag_value)
			{
				for(j = 0; j < ASSOC; j++)
				{
					if(t_set[index].counter[j] < t_set[index].counter[i])
					{
						t_set[index].counter[j]++;
					}	
				}
				#ifdef DEBUG
                        		cout<<"Read (LRU) - Cache Hit"<<endl;
				#endif
				status = false;
				t_set[index].counter[i] = 0;
				flag = 1;
				break;
                        }
                }
		if(flag == 0)
		{
			init_counter = t_set[index].counter[0];
			pos = 0;
			for(i = 0; i < ASSOC; i++)
              		{
                      		if(t_set[index].counter[i] > init_counter)
                          	{
                              		pos = i;
					init_counter = t_set[index].counter[i];
                          	}
                  	}
			nlevel_tag_value = t_set[index].tag[pos];
                  	t_set[index].tag[pos] = tag_value;
                  	for (i = 0; i < ASSOC; i++)
                  	{
                      		if(i != pos)
                          	{
                                	t_set[index].counter[i]++;
                          	}
                  	}
                  	t_set[index].counter[pos] = 0;
			if (t_set[index].dirty_bit[pos] == true)
                        {
                        	if (this->LEVEL == 1)
				{
					this->num_wb++;
					
					nlevel_tag_value = nlevel_tag_value<<INDEX;
					nlevel_tag_value += index;
					
					nlevel_index = nlevel_tag_value & (nextLevel->SETS - 1);
					nlevel_tag_value = nlevel_tag_value>>nextLevel->INDEX;
					nextLevel->write(nlevel_tag_value, nlevel_index);					
				}
				else
				{
					this->num_wb++;
				}
				t_set[index].dirty_bit[pos] = false;		
                        }

                  	#ifdef DEBUG
                      		cout<<"Read (LRU) - Cache Miss"<<endl;
			#endif
                  		status = true;
				this->read_miss++;
			
		}
		     
	}
	else
	{
		for(i = 0; i < t_set[index].valid_bit; i++)
		{
			if(t_set[index].tag[i] ==  tag_value)
                        {
                                for(j = 0; j < t_set[index].valid_bit; j++)
                                {
                                        if(t_set[index].counter[j] < t_set[index].counter[i])
                                        {
                                                t_set[index].counter[j]++;
                                        }
                                }
                                #ifdef DEBUG
                                        cout<<"Read(LRU) - Cache Hit"<<endl;
				#endif
                                status = false;
                                t_set[index].counter[i] = 0;
                                flag = 1;
                                break;
                        }

		}
		if(flag == 0)
                {
                        init_counter = t_set[index].counter[0];
			pos = 0;
                        for(i = 0; i < ASSOC; i++)
                        {
                                if(t_set[index].counter[i] > init_counter)
                                {
                                        pos = i;
					init_counter = t_set[index].counter[i];
                                }
                        }
			nlevel_tag_value = t_set[index].tag[pos];
                        t_set[index].tag[pos] = tag_value;
                        for (i = 0; i < ASSOC; i++)
                        {
                                if(i != pos)
                                {
                                        t_set[index].counter[i]++;
                                }
                        }
                        t_set[index].counter[pos] = 0;
			if (t_set[index].dirty_bit[pos] == true)
                        {
                        	if (this->LEVEL == 1)
				{
					this->num_wb++;
                                        nlevel_tag_value = nlevel_tag_value<<INDEX;
                                        nlevel_tag_value += index;
                                        nlevel_index = nlevel_tag_value & (nextLevel->SETS - 1);
                                        nlevel_tag_value = nlevel_tag_value>>nextLevel->INDEX;
                                        nextLevel->write(nlevel_tag_value, nlevel_index);
				}
				else
				{
					this->num_wb++;
				}
				t_set[index].dirty_bit[pos] = false;
                        }
                        if (pos >= t_set[index].valid_bit)
			{
				t_set[index].valid_bit++;
                        }
			#ifdef DEBUG
                                cout<<"Read (LRU) - Cache Miss"<<endl;
			#endif
                        	status = true;
				this->read_miss++;
			
                }	
	}
	return status;
}


bool CACHE::compute_writeback_allocate(int tag_value, int index)
{
	int i,j;
	bool status = true;
	int init_counter;
	int flag, pos;
	int nlevel_tag_value, nlevel_index;
	this->num_writes++;

	init_counter = t_set[index].counter[0];
	flag = 0, pos=0;
	if(t_set[index].valid_bit == 0)
	{
		for(i = 0; i < ASSOC; i++)
		{
			if(t_set[index].counter[i] > init_counter)
			{
				pos = i;
				init_counter = t_set[index].counter[i];
			}
		}
		t_set[index].tag[pos] = tag_value;
		for (i = 0; i < ASSOC; i++)
		{
			if(i != pos)
			{
				t_set[index].counter[i]++;
			}	
		}
		t_set[index].counter[pos] = 0;
		t_set[index].dirty_bit[pos] = true;
		t_set[index].valid_bit++;
		#ifdef DEBUG
			cout<<"Write (WBWA - LRU) - Cache Miss"<<endl;
		#endif
		status = true;
		this->write_miss++;
		 
	}
	else if(t_set[index].valid_bit == ASSOC)
	{
		for(i = 0; i < ASSOC; i++)
		{
               		if(t_set[index].tag[i] ==  tag_value)
			{
				for(j = 0; j < ASSOC; j++)
				{
					if(t_set[index].counter[j] < t_set[index].counter[i])
					{
						t_set[index].counter[j]++;
					}	
				}
			#ifdef DEBUG
                       		cout<<"Write (WBWA - LRU) - Cache Hit"<<endl;
 			        	
			#endif
				status = false;
				t_set[index].counter[i] = 0;
				t_set[index].dirty_bit[i] = true;
				flag = 1;
				break;
                       	}
               	}	
		if(flag == 0)
		{	
			init_counter = t_set[index].counter[0];
			pos = 0;
			for(i = 0; i < ASSOC; i++)
         		{
               			if(t_set[index].counter[i] > init_counter)
                       	  	{
                              		pos = i;
					init_counter = t_set[index].counter[i];
                          	}
                  	}
			nlevel_tag_value = t_set[index].tag[pos];
                  	t_set[index].tag[pos] = tag_value;
                  	for (i = 0; i < ASSOC; i++)
                  	{
                      		if(i != pos)
                          	{
                                	t_set[index].counter[i]++;
                          	}
                  	}
                  	t_set[index].counter[pos] = 0;
			if (t_set[index].dirty_bit[pos] == true)
			{
				if (this->LEVEL == 1)
				{
					this->num_wb++;
                                        nlevel_tag_value = nlevel_tag_value<<INDEX;
                                        nlevel_tag_value += index;
                                        nlevel_index = nlevel_tag_value & (nextLevel->SETS - 1);
                                        nlevel_tag_value = nlevel_tag_value>>nextLevel->INDEX;
                                        nextLevel->write(nlevel_tag_value, nlevel_index);	
				}
				else
				{
					this->num_wb++;
				}
			}
			t_set[index].dirty_bit[pos] = true;
                  	#ifdef DEBUG
                      		cout<<"Write (WBWA - LRU) - Cache Miss"<<endl;	
			#endif			
			status = true;
			this->write_miss++;
			
		}	     	
	}	
	else	
	{	
		for(i = 0; i < t_set[index].valid_bit; i++)
		{
			if(t_set[index].tag[i] ==  tag_value)
               	        {
               	                for(j = 0; j < t_set[index].valid_bit; j++)
               	                {
               	                        if(t_set[index].counter[j] < t_set[index].counter[i])
               	                        {
               	                                t_set[index].counter[j]++;
               	                        }
               	                }
               	                #ifdef DEBUG
               	                        cout<<"Write (WBWA - LRU) - Cache Hit"<<endl;
				#endif
               	                status = false;
				
               	                t_set[index].counter[i] = 0;
				t_set[index].dirty_bit[i] = true;
               	                flag = 1;
               	                break;
               	        }
		}
		if(flag == 0)
               	{
               	        init_counter = t_set[index].counter[0];
			pos = 0;
               	        for(i = 0; i < ASSOC; i++)
               	        {
               	                if(t_set[index].counter[i] > init_counter)
               	                {
               	                        pos = i;
					init_counter = t_set[index].counter[i];
               	                }
               	        }
			nlevel_tag_value = t_set[index].tag[pos];
               	        t_set[index].tag[pos] = tag_value;
               	        for (i = 0; i < ASSOC; i++)
               	        {
               	                if(i != pos)
               	                {
               	                        t_set[index].counter[i]++;
               	                }
               	        }
               	        t_set[index].counter[pos] = 0;
			if (t_set[index].dirty_bit[pos] == true)
                        {
				if (this->LEVEL == 1)
				{
					this->num_wb++;
                                        nlevel_tag_value = nlevel_tag_value<<INDEX;
                                        nlevel_tag_value += index;
                                        nlevel_index = nlevel_tag_value & (nextLevel->SETS - 1);
                                        nlevel_tag_value = nlevel_tag_value>>nextLevel->INDEX;
                                        nextLevel->write(nlevel_tag_value, nlevel_index);
				}
				else
				{
					this->num_wb++;
                        	}
			}
			t_set[index].dirty_bit[pos] = true;
                	if (pos >= t_set[index].valid_bit)
			{
				t_set[index].valid_bit++;
                	}
			#ifdef DEBUG
                		cout<<"Write (WBWA - LRU) - Cache Miss"<<endl;
			#endif
			status = true;
			this->write_miss++;
			
                }
	}
	return status;
}


void TAG_SET::initialise_tagset()
{
	int j;
	for(j = 0; j < ASSOC; j++)
	{
		this->counter[j] = ASSOC-1-j;
                this->tag[j] = -1;
                this->dirty_bit[j] = false;
	}
	this->valid_bit = 0;
}

void CACHE::initialise_tagstore()
{
	int i;
	for(i = 0; i < SETS; i++)
	{
		t_set[i].initialise_tagset();
	}
}

void STREAMBUFFER::initialise_onestbuf()
{
	int j;
        for(j = 0; j < PREF_M; j++)
        {
                this->stream_tag[j] = -1;
                this->invalid_bit[j] = -1;
        }
        this->valid_bit_stbuf = 0;
}

void CACHE::initialise_streambuffers()
{
	int i;
	for(i = 0; i < PREF_N; i++)
	{
		stbuf[i].initialise_onestbuf();
		stbuf[i].LRU_counter = i;
	}
	
		
}


CACHE::CACHE(int BLOCKSIZE_t, int SIZE_t, int ASSOC_t, int PREF_N_t, int PREF_M_t, string t_file, int LEVEL_t)
{
	BLOCKSIZE = BLOCKSIZE_t;
	SIZE = SIZE_t;
        ASSOC = ASSOC_t;
	PREF_N = PREF_N_t;
	PREF_M = PREF_M_t;
        trace_file = t_file;
	LEVEL = LEVEL_t;
}


void CACHE::display_cache_config()
{
        cout<<" ===== Simulator configuration ====="<<endl;
        cout<<" BLOCKSIZE:                    "<<BLOCKSIZE<<endl;
        cout<<" L1_SIZE:                       "<<SIZE<<endl;
        cout<<" L1_ASSOC:                         "<<ASSOC<<endl;
        cout<<" trace_file:           "<<trace_file<<endl;
        cout<<" ==================================="<<endl<<endl;
}


void CACHE::compute_sets()
{
        SETS = SIZE / (BLOCKSIZE * ASSOC);
	#ifdef DEBUG
		cout<<"The no. of sets is "<<SETS<<endl;
	#endif
}


void CACHE::compute_index()
{
        INDEX = log2(SETS);
	#ifdef DEBUG
		cout<<"The index is "<<INDEX<<endl;
	#endif
}


void CACHE::compute_blockoffset()
{
	BLOCK_OFFSET = log2(BLOCKSIZE);
	#ifdef DEBUG
		cout<<"The block offset is "<<BLOCK_OFFSET<<endl;
	#endif
}


void CACHE::compute_tagsize()
{
	TAG_SIZE = 32 - INDEX - BLOCK_OFFSET;
	#ifdef DEBUG
		cout<<"The tag data size is "<<TAG_SIZE<<endl;
        #endif
}


int CACHE::manipulate_address(string address, int &index_l)
{
	int addr;
	int index_temp;
	addr = strtol(address.c_str(), NULL, 16);	
	addr = addr>>BLOCK_OFFSET;
	index_temp = (SETS - 1);
	index_l = addr & (index_temp);
	addr = addr>>INDEX;
	return addr; 
}

double CACHE::compute_aat()
{
	double L1_miss_penalty;
	double L1_hit_time;
	double L1_miss_rate;
	double L2_miss_penalty;
        double L2_hit_time;
        double L2_miss_rate;
	double aat;
	L1_miss_penalty = (double) (20.0 + (0.5 * ((double)(BLOCKSIZE) / (double)(16))));
	cout<<"L1 miss penalty "<<L1_miss_penalty<<endl;
	L1_hit_time = (double) (0.25 + (2.5 * ((double) (SIZE) / (double) (512 * 1024))) + (0.025 * ((double) (BLOCKSIZE)  / (double)16)) + (0.025 * ASSOC));
	cout<<"L1 hit time "<<L1_hit_time<<endl;
	L1_miss_rate = ((double) (this->read_miss + this->write_miss) / (double) (this->num_reads+this->num_writes));
	cout<<"L1 miss rate "<<L1_miss_rate<<endl;
	L2_miss_penalty = (double) (20.0 + (0.5 * ((double)(this->nextLevel->BLOCKSIZE) / (double)(16))));
        cout<<"L2 miss penalty "<<L2_miss_penalty<<endl;
	L2_hit_time = (double) (2.5 + (2.5 * ((double) (this->nextLevel->SIZE) / (double) (512 * 1024))) + (0.025 * ((double) (this->nextLevel->BLOCKSIZE)  / (double)16)) + (0.025 * this->nextLevel->ASSOC));
        cout<<"L2 hit time "<<L2_hit_time<<endl;
	L2_miss_rate = ((double) (this->nextLevel->read_miss) / (double) (this->nextLevel->num_reads));
	cout<<"L2 miss mate "<<L2_miss_rate<<endl;

	aat = (double) (L1_hit_time + (L1_miss_rate * ((L2_hit_time + (L2_miss_rate * L2_miss_penalty)))));
	return aat;
}


void CACHE::print_cache()
{
	int i,j;
	cout<<"===== L1 contents ====="<<endl;
	for (i = 0; i < SETS; i++)
	{
		cout<<"set  "<<i<<": ";
		for(j = 0; j < ASSOC; j++)
		{
			cout<<"   "<<std::hex<<t_set[i].tag[j];
			printf("   %x",t_set[i].tag[j]);
			if (t_set[i].dirty_bit[j] == true)
				cout<<"D";
		}
		cout<<endl;
	}
	cout<<endl;
}

void CACHE::print_cache_order()
{
        int i,j,count;
        for (i = 0; i < SETS; i++)
        {
                count = 0;
		cout<<"Set  "<<i<<": ";
		while(count < this->ASSOC)
		{
                	for(j = 0; j < this->ASSOC; j++)
                	{
                  		if (count == t_set[i].counter[j])
				{      
					printf("   %x",t_set[i].tag[j]);
                        		if (t_set[i].dirty_bit[j] == true)
                                		cout<<"D";
					count++;
				}
                	}
		}
                cout<<endl;
        }
}


void CACHE::print_stats()
{
	cout<<"===== Simulation results (raw) ====="<<endl;
 	cout<<"a. number of L1 reads:           "<<this->num_reads<<endl;
  	cout<<"b. number of L1 read misses:     "<<this->read_miss<<endl;
  	cout<<"c. number of L1 writes:          "<<this->num_writes<<endl;
  	cout<<"d. number of L1 write misses:    "<<this->write_miss<<endl;
	
	printf("e. L1 miss rate:                 %4.6lf\n", ((double) (this->read_miss + this->write_miss) / (double) (this->num_reads+this->num_writes)));
	cout<<"f. number of L1 writebacks:     "<<this->num_wb<<endl;
	
	if (this->PREF_M == 0)
		cout<<"g. number of L1 prefetches:     0"<<endl;  	
	else
	{
	}
	cout<<"h. number of L2 reads that did not originate from L1 prefetches:   "<<this->nextLevel->num_reads<<endl;
	cout<<"i. number of L2 read misses that did not originate from L1 prefetches:  "<<this->nextLevel->read_miss<<endl;

	if (this->PREF_M == 0)
        	cout<<"j. number of L2 reads that originated from L1 prefetches:              0"<<endl;
	else
        {
        }
	
	if (this->PREF_M == 0)
        	cout<<"k. number of L2 read misses that originated from L1 prefetches:        0"<<endl;
	else
        {
        }
	cout<<"l. number of L2 writes:         "<<this->nextLevel->num_writes<<endl;
	cout<<"m. number of L2 write misses:   "<<this->nextLevel->write_miss<<endl;
	printf("n. L2 miss rate:                 %4.6lf\n", (double)(((double)this->nextLevel->read_miss)/((double)this->nextLevel->num_reads)));
	cout<<"o. number of L2 writebacks:     "<<this->nextLevel->num_wb<<endl;
	cout<<"p. total memory traffic:          "<<this->nextLevel->read_miss+this->nextLevel->write_miss+this->nextLevel->num_wb+this->nextLevel->wt_memt<<endl;

	printf("q. average access time:         %4.6lf\n", (double)compute_aat());

}
