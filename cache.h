#ifndef SIM_CACHE_H
#define SIM_CACHE_H

#include<iostream>
#include<string>

using namespace std;

class CACHE;

class TAG_SET
{
	private :
		int *tag;
		int *counter;
		bool *dirty_bit;
		int valid_bit;
		int ASSOC;
	public :
		TAG_SET() {}
		void create_tagset(int ASSOC_t);
		void initialise_tagset();
};

class STREAMBUFFER
{
	private :
		int PREF_M;
		int *stream_tag;
		int *invalid_bit;
		int valid_bit_stbuf;
		int LRU_counter;
		CACHE *nextLevel;
	public :
		STREAMBUFFER() {}
		void create_streambuffer(int PREF_M_t);
		void initialise_onestbuf();
};


class CACHE
{
        private :
                int BLOCKSIZE, SIZE, ASSOC, REPLACEMENT_POLICY, WRITE_POLICY;
                int INDEX, BLOCK_OFFSET, SETS, TAG_SIZE, LEVEL;
		int PREF_M, PREF_N;
		long int read_miss, write_miss, num_wb, num_reads, num_writes, wt_memt;
		string trace_file;
		TAG_SET *t_set;
		STREAMBUFFER *stbuf;
		CACHE *nextLevel;
        public:
                CACHE() {}
                CACHE(int BLOCKSIZE_t, int SIZE_t, int ASSOC_t, int PREF_N_t, int PREF_M_t, string t_file, int level);
                void allocate_tag_set();
		void allocate_streambuffers();
		bool write(int index_l, int tag_value);
		bool read(int index_l, int tag_value);
		bool compute_LRU(int tag_value, int index);
                bool compute_writeback_allocate(int tag_value, int index);
		bool compute_streambuffer(int tag_value, int index);
		void display_cache_config();
		void compute_blockoffset();
		void compute_sets();
		void compute_index();
		void compute_tagsize();
		void initialise_tagstore();
		void initialise_streambuffers();
		int manipulate_address(string address, int &index_l);
		void initialise_stats();
		// Print 
		void print_stats();
		void print_cache();
		void print_cache_order();
		double compute_aat();
};

#endif
